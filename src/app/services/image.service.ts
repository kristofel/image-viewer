import { Inject, Injectable } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';

export interface Image {
  title: string;
  description: string;
  thumbnail: string;
  url: string;
  id: string;
  favorite: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  private storageKey = 'local_images';

  constructor(@Inject(SESSION_STORAGE) private storage: StorageService<Image[]>) { }

  public saveImagesList(imagesList: Image[]): void {
    this.storage.set(this.storageKey, imagesList);
    const images = this.getImagesList();
    console.log("images after: ", images);
  }

  public getImagesList(): Image[] | undefined {
    return this.storage.get(this.storageKey);
  }

  public getImageById(imageId: string): Image | undefined {
    const images = this.storage.get(this.storageKey);

    if (!images) {
      return undefined;
    }

    return images.find(image => image.id === imageId);
  }

  public setImageById(imageId: string, updatedImage: Image): void {
    let images = this.storage.get(this.storageKey);

    if (!images) {
      return;
    }

    let imageIndex = images.findIndex(image => image.id === imageId);

    if (imageIndex === -1) {
      return;
    }

    images[imageIndex] = updatedImage;
    this.saveImagesList(images);
  }


  public createImageDatabase() {
    if (this.getImagesList() !== undefined) {
      return;
    }

    let images: Image[] = [];

    const names = [
      "ant", "bee", "chimpanzee", "duck", "emu",
      "ferret", "giraffe", "hare", "iguana", "jackal",
      "koala", "lion", "moth", "narwhal", "octopus"
    ];

    names.forEach((name, index) => images.push(
      {
        title: `image of ${name}`,
        description: `Lorem ${name} ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        thumbnail: `../../../assets/thumbnails/${index + 1}.jpg`,
        url: `../../../assets/images/${index + 1}.jpg`,
        id: `${index + 1}`,
        favorite: false,
      }
    ))

    console.log("images: ", images);


    this.saveImagesList(images);
  }
}
