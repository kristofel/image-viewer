import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ImageService, Image } from 'src/app/services/image.service';

@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.scss']
})
export class ImageListComponent implements OnInit {

  public images: Image[] = [];
  public favorites = false;
  public searchedText = "";
  public allImagesCount = 0;

  constructor(private imageService: ImageService, private router: Router) { }

  ngOnInit(): void {
    console.log("ImageListComponent on init");

    this.imageService.createImageDatabase();
    this.getImageList();
  }

  getImageList() {
    const images = this.imageService.getImagesList();
    if (images) {
      this.allImagesCount = images.length;
      if (this.favorites === true) {
        this.images = images.filter(image => image.favorite === true);
      } else {
        this.images = images;
      }
      if (this.searchedText) {
        this.images = this.images.filter(image => image.title.includes(this.searchedText) || image.id.includes(this.searchedText));
      }
    }
  }

  goToPreview(imageId: string) {
    this.router.navigate(['/images', imageId],);
  }

  toggleFavorites() {
    this.favorites = !this.favorites;
    this.getImageList();
  }

  addToFavorites(imageId: string) {
    let image = this.imageService.getImageById(imageId);

    if (image) {
      image.favorite = !image.favorite;
      this.imageService.setImageById(imageId, image);
      this.getImageList();
    }
  }
}
