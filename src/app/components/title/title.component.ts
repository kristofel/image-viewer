import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent {
  constructor(private router: Router) { }

  goToHomePage() {
    this.router.navigate(['/'],);
  }
}
