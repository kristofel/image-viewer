import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Image, ImageService } from 'src/app/services/image.service';

@Component({
  selector: 'app-image-preview',
  templateUrl: './image-preview.component.html',
  styleUrls: ['./image-preview.component.scss']
})
export class ImagePreviewComponent implements OnInit {
  public image!: Image;

  constructor(private imageService: ImageService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.getImage();
  }

  getImage() {
    const imageId = this.activatedRoute.snapshot.paramMap.get("id");

    if (imageId !== null && imageId !== undefined) {
      const image = this.imageService.getImageById(imageId);
      if (image) {
        this.image = image;
      }
    }
  }

  addToFavorites(imageId: string) {
    let image = this.imageService.getImageById(imageId);

    if (image) {
      image.favorite = !image.favorite;
      this.imageService.setImageById(imageId, image);
      this.image = image;
    }
  }
}
